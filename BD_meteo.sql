-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 28-Jan-2020 às 13:41
-- Versão do servidor: 10.4.8-MariaDB
-- versão do PHP: 7.3.10

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `tp_meteo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `alerta`
--

CREATE TABLE `alerta` (
  `id` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `descricao` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `alerta`
--

INSERT INTO `alerta` (`id`, `tipo`, `descricao`) VALUES
(1, 'Verde', 'Não se prevê nenhuma situação meteorológica de risco.'),
(2, 'Amarelo', 'Situação de risco para determinadas atividades dependentes da situação meteorológica.'),
(3, 'Laranja', 'Situação meteorológica de risco moderado a elevado.'),
(4, 'Vermelho', 'Situação meteorológica de risco extremo.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `elemento`
--

CREATE TABLE `elemento` (
  `id` int(11) NOT NULL,
  `tipo` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `elemento`
--

INSERT INTO `elemento` (`id`, `tipo`) VALUES
(1, 'tempAr'),
(2, 'humidAr'),
(3, 'velocVento'),
(4, 'dirVento'),
(5, 'indUV'),
(6, 'tempMar'),
(7, 'dirMar'),
(8, 'elevMar'),
(9, 'pluviosidade');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estacao`
--

CREATE TABLE `estacao` (
  `id` int(11) NOT NULL,
  `nome` tinytext NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `elevacao` int(11) NOT NULL,
  `Local_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `estacao`
--

INSERT INTO `estacao` (`id`, `nome`, `latitude`, `longitude`, `elevacao`, `Local_id`) VALUES
(1, 'Universidade', 37.02, -7.95, 13, 1),
(2, 'Praia', 37, -8, 2, 1),
(3, 'Foia', 37.31, -8.98, 447, 2),
(4, 'Centro', 37.03, -8.98, 12, 3),
(5, 'Cabo', 37.02, -8.99, 15, 3),
(6, 'Centro', 38.56, -7.94, 269, 4),
(7, 'Centro', 38.74, -9.19, 102, 5),
(8, 'Torre', 40.3, -7.57, 1990, 6),
(9, 'Universidade', 40.22, -8.48, 44, 7),
(10, 'Aliados', 41.14, -8.61, 97, 8),
(11, 'Praia', 38.97, -9.41, 5, 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `local`
--

CREATE TABLE `local` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `local`
--

INSERT INTO `local` (`id`, `nome`) VALUES
(1, 'Faro'),
(2, 'Monchique'),
(3, 'Sagres'),
(4, 'Evora'),
(5, 'Lisboa'),
(6, 'Covilha'),
(7, 'Coimbra'),
(8, 'Porto'),
(9, 'Ericeira');

-- --------------------------------------------------------

--
-- Estrutura da tabela `observacao`
--

CREATE TABLE `observacao` (
  `id` int(11) NOT NULL,
  `data` datetime(6) NOT NULL,
  `valor` varchar(10) NOT NULL,
  `Estacao_id` int(11) NOT NULL,
  `Alerta_id` int(11) NOT NULL,
  `Elemento_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `observacao`
--

INSERT INTO `observacao` (`id`, `data`, `valor`, `Estacao_id`, `Alerta_id`, `Elemento_id`) VALUES
(1, '2020-01-14 00:00:00.000000', '7', 1, 1, 1),
(2, '2020-01-14 00:00:00.000000', '78', 1, 1, 2),
(3, '2020-01-14 00:00:00.000000', '11', 1, 1, 3),
(4, '2020-01-14 00:00:00.000000', 'NO', 1, 1, 4),
(5, '2020-01-14 00:00:00.000000', '1', 1, 1, 5),
(6, '2020-01-14 00:00:00.000000', '0', 1, 1, 9),
(7, '2020-01-14 00:00:00.000000', '14', 2, 1, 6),
(8, '2020-01-14 00:00:00.000000', 'SE', 2, 1, 7),
(9, '2020-01-14 00:00:00.000000', '2', 2, 2, 8),
(10, '2020-01-14 00:00:00.000000', '3', 3, 2, 1),
(11, '2020-01-14 00:00:00.000000', '91', 3, 2, 2),
(12, '2020-01-14 00:00:00.000000', '8', 3, 1, 3),
(13, '2020-01-14 00:00:00.000000', 'SO', 3, 1, 4),
(14, '2020-01-14 00:00:00.000000', '0', 3, 1, 5),
(15, '2020-01-14 00:00:00.000000', '1.3', 3, 1, 9),
(16, '2020-01-14 00:00:00.000000', '4', 4, 2, 1),
(17, '2020-01-14 00:00:00.000000', '83', 4, 1, 2),
(18, '2020-01-14 00:00:00.000000', '15', 4, 2, 3),
(19, '2020-01-14 00:00:00.000000', 'SO', 4, 1, 4),
(20, '2020-01-14 00:00:00.000000', '0', 4, 1, 5),
(21, '2020-01-14 00:00:00.000000', '1.2', 4, 1, 9),
(22, '2020-01-14 00:00:00.000000', '13', 5, 1, 6),
(23, '2020-01-14 00:00:00.000000', 'SE', 5, 1, 7),
(24, '2020-01-14 00:00:00.000000', '3', 5, 3, 8),
(25, '2020-01-14 00:00:00.000000', '6', 6, 1, 1),
(26, '2020-01-14 00:00:00.000000', '75', 6, 1, 2),
(27, '2020-01-14 00:00:00.000000', '3', 6, 1, 3),
(28, '2020-01-14 00:00:00.000000', 'SE', 6, 1, 4),
(29, '2020-01-14 00:00:00.000000', '0', 6, 1, 5),
(30, '2020-01-14 00:00:00.000000', '0', 6, 1, 9),
(31, '2020-01-14 00:00:00.000000', '4', 7, 1, 1),
(32, '2020-01-14 00:00:00.000000', '77', 7, 1, 2),
(33, '2020-01-14 00:00:00.000000', '3', 7, 1, 3),
(34, '2020-01-14 00:00:00.000000', 'SE', 7, 1, 4),
(35, '2020-01-14 00:00:00.000000', '0', 7, 1, 5),
(36, '2020-01-14 00:00:00.000000', '1.2', 7, 2, 9),
(37, '2020-01-14 00:00:00.000000', '-3', 8, 3, 1),
(38, '2020-01-14 00:00:00.000000', '82', 8, 2, 2),
(39, '2020-01-14 00:00:00.000000', '14', 8, 3, 3),
(40, '2020-01-14 00:00:00.000000', 'S', 8, 1, 4),
(41, '2020-01-14 00:00:00.000000', '0', 8, 1, 5),
(42, '2020-01-14 00:00:00.000000', '2.3', 8, 3, 9),
(43, '2020-01-14 00:00:00.000000', '4', 9, 1, 1),
(44, '2020-01-14 00:00:00.000000', '80', 9, 2, 2),
(45, '2020-01-14 00:00:00.000000', '10', 9, 2, 3),
(46, '2020-01-14 00:00:00.000000', 'N', 9, 1, 4),
(47, '2020-01-14 00:00:00.000000', '0', 9, 1, 5),
(48, '2020-01-14 00:00:00.000000', '1,5', 9, 2, 9),
(49, '2020-01-14 00:00:00.000000', '5', 10, 1, 1),
(50, '2020-01-14 00:00:00.000000', '82', 10, 2, 2),
(51, '2020-01-14 00:00:00.000000', '11', 10, 2, 3),
(52, '2020-01-14 00:00:00.000000', 'N', 10, 1, 4),
(53, '2020-01-14 00:00:00.000000', '0', 10, 1, 5),
(54, '2020-01-14 00:00:00.000000', '1,5', 11, 2, 9),
(55, '2020-01-14 00:00:00.000000', '11', 11, 2, 6),
(56, '2020-01-14 00:00:00.000000', 'SE', 11, 1, 7),
(57, '2020-01-14 00:00:00.000000', '2', 11, 2, 8);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `alerta`
--
ALTER TABLE `alerta`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `elemento`
--
ALTER TABLE `elemento`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `estacao`
--
ALTER TABLE `estacao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Estacao_Local_idx` (`Local_id`);

--
-- Índices para tabela `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `observacao`
--
ALTER TABLE `observacao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Observacao_Estacao1_idx` (`Estacao_id`),
  ADD KEY `fk_Observacao_Alerta1_idx` (`Alerta_id`),
  ADD KEY `fk_Observacao_Elemento1_idx` (`Elemento_id`);

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `estacao`
--
ALTER TABLE `estacao`
  ADD CONSTRAINT `fk_Estacao_Local` FOREIGN KEY (`Local_id`) REFERENCES `local` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `observacao`
--
ALTER TABLE `observacao`
  ADD CONSTRAINT `fk_Observacao_Alerta1` FOREIGN KEY (`Alerta_id`) REFERENCES `alerta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Observacao_Elemento1` FOREIGN KEY (`Elemento_id`) REFERENCES `elemento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Observacao_Estacao1` FOREIGN KEY (`Estacao_id`) REFERENCES `estacao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
