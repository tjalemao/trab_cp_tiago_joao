"""
TP - Complementos Programacao
Tiago Santos - a64137
Joao Ventura - a19919
"""

#definição de localhost e porta da API

import os
from app import app

if __name__ == "__main__":
    app.debug = True
    host = os.environ.get('IP', '0.0.0.0')
    port = int(os.environ.get('PORT', 8080))
    app.run(host=host, port=port)